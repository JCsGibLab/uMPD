/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import QtQuick.Controls.Suru 2.2

Item {
    id: errorMsgItem
    
    property alias errorMsg: labelError.text
    
    Rectangle {
        width: parent.width
        height: parent.height
        color: Suru.backgroundColor
    }
    
    Label {
        id: labelError
        width: parent.width
        height: parent.height
        
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        
        font.pointSize: units.gu(2)
        wrapMode: Text.WordWrap
        color: "red"
    }
}
