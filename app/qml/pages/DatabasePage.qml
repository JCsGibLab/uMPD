/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.Controls.Suru 2.2
import "../components"

Page {
    id: databasePage

    property bool contentView: false
    property bool searching: false
    property var listName: ""
    property int playlistPos: 0
    
    onSearchingChanged: if (searching === false) {
                          searchField.text = ""
    }

    header: PageHeader {
        title: contentView ? i18n.tr("Back") : i18n.tr("Database")
        id: header

        leadingActionBar.actions: [
            Action {
                iconName: "back"
                text: i18n.tr('Back')
                onTriggered: {
                    if ( searching || contentView ) {
                      searching = false
                      contentView = false
                      if(tabView.currentIndex === 1) mpd.playlist.checkPlaylists()
                      if(tabView.currentIndex === 2) mpd.search.getAlbumList()
                      if(tabView.currentIndex === 3) mpd.search.getArtistList()
                    } else {
                      pageStack.pop()
                    }
                }
            }
        ]
        
        trailingActionBar.actions: [
            Action {
                id: accountIcon
                visible: !searching
                objectName: "infoIcon"
                text: i18n.tr("About")
                iconName: "info"
                onTriggered: {
                  push( Qt.resolvedUrl("./AboutPage.qml") )
                }
            },
            Action {
                id: updateIcon
                visible: !searching
                objectName: "updateIcon"
                text: i18n.tr("Update")
                iconName: "sync"
                onTriggered: {
                  mpd.stats.update()
                  PopupUtils.open(updateDatabase)
                }
            },
            Action {
                id: searchIcon
                visible: tabView.currentIndex >= 2 && !searching && !contentView
                objectName: "searchIcon"
                text: i18n.tr("Search")
                iconName: "edit-find"
                onTriggered: {
                  searchField.focus = searching = true
                }
            }
        ]
        
        states: [
        State {
            name: "searching"
            when: searching
            PropertyChanges {
                target: header
                contents: searchField
            }
        }
        ]

        
        extension: Sections {
            id: sections
            anchors {
                horizontalCenter: parent.horizontalCenter
            }

            model: [
                i18n.tr("Folder"),
                i18n.tr("Playlists"),
                i18n.tr("Albums"),
                i18n.tr("Artists")
            ]

            onSelectedIndexChanged: tabView.currentIndex = selectedIndex

            StyleHints {
                sectionColor: "#88FFFFFF"
                selectedSectionColor: "White"
                underlineColor: "Transparent"
                pressedBackgroundColor: "#76C8C4"
            }
        }

        StyleHints {
            foregroundColor: "#FFF"
            backgroundColor: UbuntuColors.orange
            dividerColor: "#85D8CE"
        }
        
    }

    TextField {
        id: searchField
        objectName: "searchField"
        visible: searching
        primaryItem: Icon {
            height: parent.height - units.gu(2)
            name: "find"
            anchors.left: parent.left
            anchors.leftMargin: units.gu(0.25)
        }
        anchors {
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        width: parent.width
        inputMethodHints: Qt.ImhNoPredictiveText
        placeholderText: tabView.currentIndex === 2 ? i18n.tr("Search by album...") : i18n.tr("Search by artist...")
        
        onDisplayTextChanged: {
          if( searching && tabView.currentIndex === 2 ){
            mpd.search.forAlbum(text)
          } else if ( searching && tabView.currentIndex === 3 ){
            mpd.search.forArtist(text)
          }
        }
    }

    WaitingBar {
        id: waitingBar
        anchors.top: header.bottom
        connectionState: mpd.profile.isConnected
        z: 10
    }

    Component {
        id: updateDatabase
	
        Dialog {
            id: updateDatabaseDialog
            title: i18n.tr("Database statistic")
           
            Label { 
                text: i18n.tr("Last Database update:") + " " + mpd.stats.updateTime
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
            
            Label { 
                text: i18n.tr("Number of Artists:") + " " + mpd.stats.numberOfArtists
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
            
            Label { 
                text: i18n.tr("Number of Albums:") + " " + mpd.stats.numberOfAlbums
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
            
            Label { 
                text: i18n.tr("Number of Songs:") + " " + mpd.stats.numberOfSongs
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
            
            Label { 
                text: i18n.tr("Total playtime:") + " " + mpd.stats.playTime
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
           
            Button {
                id: updateButton
                text: i18n.tr("Update database")
                color: UbuntuColors.green
                onClicked:
                {
                  if( mpd.database.update() ) {
                    updateButton.text = i18n.tr("Update done")
                    updateButton.color = UbuntuColors.ash
                    mpd.stats.update()
                  } else {
                    updateButton.color = UbuntuColors.ash
                    updateButton.text = i18n.tr("Update failed")
                  }
                }
            }
            
            Button {
                text: i18n.tr("back")
                color: UbuntuColors.ash
                onClicked: PopupUtils.close(updateDatabaseDialog)
            }
        }
    }
    
    VisualItemModel {
        id: tabs
//-------------------------------------------------------------------------------------------------
/* Folder */
//-------------------------------------------------------------------------------------------------
        Item {
            width: tabView.width
            height: tabView.height
            opacity: tabView.currentIndex === 0 ? 1 : 0

            Behavior on opacity {
                NumberAnimation { duration: 200; easing.type: Easing.InOutCubic }
            }

            Rectangle {
              id: rect
              anchors.top: parent.top
              z: 1
              width: parent.width
              height: navView.height + units.gu(1)
              color: Suru.backgroundColor
            }

            ListView {
                id: navView
                z: 2
                width: parent.width
                height: units.gu(4)
                anchors.top: parent.top
                anchors.topMargin: units.gu(0.5)
                orientation: ListView.Horizontal
                
                currentIndex: mpd.database.navPos
                
                model: mpd.database.navmodel
                
                delegate: ListItem {

                    id: modelItem
                    height: modelLayout.height + (divider.visible ? divider.height : 0)
                    width: modelLayout.width
                    
                    ListItemLayout {
                        id: modelLayout
                        width: homeIcon.width + navText.width
                        height: homeIcon.height + units.gu(1)
            
                        Icon {
                            id: homeIcon
                            visible: model.title === "Home"
                            anchors.left: modelLayout.left
                            anchors.verticalCenter: modelLayout.verticalCenter
                            width: units.gu(3)
                            height: units.gu(3)
                            color: Suru.foregroundColor
                            name: "home"
                        }
                            
                        Text {
                            id: navText
                            visible: model.title != "Home"
                            width: text.width
                            anchors.left: modelLayout.left
                            anchors.verticalCenter: modelLayout.verticalCenter
                            font.pointSize: units.gu(1.0)
                            color: Suru.foregroundColor
                            text: model.title
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: mpd.database.getActDir(model.uid, model.uri)
                    }
                }
                highlight: Rectangle { color: Suru.neutralColor; radius: units.gu(1) }
                highlightMoveDuration: 10
                highlightMoveVelocity: -1
                focus: true
            }
            
            ListView {
                id: directoryView
                width: parent.width
                height: parent.height - navView.height - units.gu(1)
                anchors.top: navView.bottom
                anchors.topMargin: units.gu(0.5)
                    
                model: mpd.database.dirmodel
                      
                delegate: ListItem {

                    height: modelLayout.height + (divider.visible ? divider.height : 0)
                      
                    ListItemLayout {
                        id: modelLayout
                        Row {
                            id: row
                            spacing: units.gu(1)
                            
                            Icon {
                                width: units.gu(3)
                                height: units.gu(3)
                                color: Suru.foregroundColor
                                source: model.uid == 1 ? "file:///usr/share/icons/suru/actions/scalable/stock_music.svg" : "file:///" + applicationDirPath + "/assets/folder.svg"
                            }
                            
                            Text {
                                anchors.verticalCenter: row.verticalCenter
                                color: Suru.foregroundColor
                                text: model.title
                            }
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: if(model.uid == 0) mpd.database.getActDir(model.uri)
                    }
                    
                    
                    trailingActions: ListItemActions {
                        actions: [
                            Action {
                                iconName: "media-playback-start"
                                text: i18n.tr("Play")
                                onTriggered: {
                                  mpd.queue.clear()
                                  if(mpd.queue.add(model.uri)) {
                                    rectUserInfo.userInfoText = i18n.tr("Add to the queue and start playing")
                                    mpd.control.play(1)
                                  } else {
                                    rectUserInfo.userInfoText = i18n.tr("Load and playback not possible")
                                  }
                                  infoTimer.start()
                                  rectUserInfo.showLabel = true                   
                                }
                            },
                            Action {
                                iconName: "media-playlist"
                                text: i18n.tr("Add")
                                onTriggered: {
                                  if(mpd.queue.add(model.uri)) {
                                    rectUserInfo.userInfoText = i18n.tr("Add to the queue")
                                  } else {
                                    rectUserInfo.userInfoText = i18n.tr("Not able to add to queue")
                                  }
                                  infoTimer.start()
                                  rectUserInfo.showLabel = true
                                }
                            }
                        ]
                    }
                }
            }
        }
        
//-------------------------------------------------------------------------------------------------
/* Playlists */
//-------------------------------------------------------------------------------------------------
        Item {
            width: tabView.width
            height: tabView.height
            opacity: tabView.currentIndex === 1 ? 1 : 0
            
            Behavior on opacity {
                NumberAnimation { duration: 200; easing.type: Easing.InOutCubic }
            }

            Item {
              id: itemPlaylistNavBar
              z: 1
              anchors.top: parent.top
              
              visible: contentView
              
              width: parent.width
              height: units.gu(6)
              
              property var widthLabelPlaylist: parent.width - units.gu(8)

              Rectangle {
                  width: parent.width
                  height: parent.height
                  color: UbuntuColors.orange
              }
              
              Row { //The "Row" type lays out its child items in a horizontal line
                spacing: units.gu(0.5)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                
                Icon {
                    width: units.gu(3)
                    height: units.gu(3)
                    color: Suru.foregroundColor
                    source: "file:///usr/share/icons/suru/mimetypes/scalable/text-x-generic-symbolic.svg"
                }

                Label {
                    id: labelActPlaylist
                      
                    width: itemPlaylistNavBar.widthLabelPlaylist
                    height: units.gu(3)
                    
                    color: Suru.foregroundColor
                    
                    verticalAlignment: Text.AlignVCenter

                    text: mpd.playlist.name
                }
              }
            }

            ListView {
                id: playlistsView
                width: parent.width
                height: contentView ? parent.height - itemPlaylistNavBar.height : parent.height
                anchors.top: contentView ? itemPlaylistNavBar.bottom : parent.top
                currentIndex: mpd.playlist.selPlaylist
                    
                model: mpd.playlist.playmodel
                      
                delegate: ListItem {

                    height: modelLayout.height + (divider.visible ? divider.height : 0)
                          
                    ListItemLayout {
                        id: modelLayout
                        Row {
                            id: row
                            spacing: units.gu(1)
                            
                            Icon {
                                width: units.gu(3)
                                height: units.gu(3)
                                color: Suru.foregroundColor
                                source: model.uid == 1 ? "file:///usr/share/icons/suru/actions/scalable/stock_music.svg" : "file:///usr/share/icons/suru/mimetypes/scalable/text-x-generic-symbolic.svg"
                            }
                            
                            Column {
                                id: column
                                Text {
                                    color: Suru.foregroundColor
                                    text: model.title
                                }
                                Text {
                                    color: Suru.foregroundColor
                                    text: model.uri
                                    font.pointSize: units.gu(1)
                                }
                            }
                        }
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if(!contentView) mpd.playlist.selectedEntity(model.title, model.uid)
                            contentView = true
                            searching = false
                        }
                    }

                    leadingActions: ListItemActions {
                        actions: [
                            Action {
                                iconName: "delete"
                                text: i18n.tr("delete")
                                onTriggered: {
                                    if(contentView) {
                                        listName = mpd.playlist.name
                                        playlistPos = model.pos
                                    } else {
                                        listName = model.title
                                    }
                                    PopupUtils.open(removePlaylist)
                                }
                            }
                        ]
                    }

                    trailingActions: ListItemActions {
                        actions: [
                            Action {
                                iconName: "media-playback-start"
                                text: i18n.tr("Play")
                                onTriggered: {
                                    mpd.queue.clear()
                                    if(contentView) {
                                        if(mpd.queue.add(model.uri)) {
                                          rectUserInfo.userInfoText = i18n.tr("Add to the queue and start playing")
                                          mpd.control.play(1)
                                        } else {
                                          rectUserInfo.userInfoText = i18n.tr("Load and playback not possible")
                                        }
                                    } else {
                                        if(mpd.playlist.addToQueue(model.title)) {
                                            rectUserInfo.userInfoText = i18n.tr("Play playlist")
                                            mpd.control.play(1)
                                        } else {
                                            rectUserInfo.userInfoText = i18n.tr("Playlist could not be played back")
                                        }
                                    }
                                    infoTimer.start()
                                    rectUserInfo.showLabel = true                          
                                }
                            },
                            Action {
                                iconName: "media-playlist"
                                text: i18n.tr("Add")
                                onTriggered: {
                                  if(contentView) {
                                      if(mpd.queue.add(model.uri)) {
                                        rectUserInfo.userInfoText = i18n.tr("Add to the queue")
                                      } else {
                                        rectUserInfo.userInfoText = i18n.tr("Not able to add to queue")
                                      }
                                    } else {
                                        if(mpd.playlist.addToQueue(model.title)) {
                                            rectUserInfo.userInfoText = i18n.tr("Playlist add to the queue")
                                        } else {
                                            rectUserInfo.userInfoText = i18n.tr("Playlist could not be added to playlist")
                                        }
                                    }
                                    infoTimer.start()
                                    rectUserInfo.showLabel = true
                                }
                            }
                        ]
                    }
                }
                highlight: Rectangle { color: Suru.neutralColor; radius: 5 }
                highlightMoveDuration: 10
                highlightMoveVelocity: -1
                focus: true
            }
        }
        
//-------------------------------------------------------------------------------------------------        
/* Albums */
//-------------------------------------------------------------------------------------------------
        Item {
            width: tabView.width
            height: tabView.height
            opacity: tabView.currentIndex === 2 ? 2 : 0

            Behavior on opacity {
                NumberAnimation { duration: 200; easing.type: Easing.InOutCubic }
            }
            
            Item {
              id: itemAlbumNavBar
              z: 1
              anchors.top: parent.top
              
              visible: contentView
              
              width: parent.width
              height: units.gu(6)
              
              property var widthLabelAlbum: parent.width - units.gu(8)

              Rectangle {
                  width: parent.width
                  height: parent.height
                  color: UbuntuColors.orange
              }
              
              Row { //The "Row" type lays out its child items in a horizontal line
                spacing: units.gu(0.5)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                
                Icon {
                    width: units.gu(3)
                    height: units.gu(3)
                    color: Suru.foregroundColor
                    source: "file:///usr/share/icons/suru/devices/scalable/media-optical-symbolic.svg"
                }

                Label {
                    id: labelActAlbum
                      
                    width: itemAlbumNavBar.widthLabelAlbum
                    height: units.gu(3)
                    
                    color: Suru.foregroundColor
                    
                    verticalAlignment: Text.AlignVCenter

                    text: mpd.search.Name
                }
              }
            }
            
            ListView {
                id: albumsView
                width: parent.width
                height: contentView ? parent.height - itemAlbumNavBar.height : parent.height
                anchors.top: contentView ? itemAlbumNavBar.bottom : parent.top
                    
                model: mpd.search.albummodel
                      
                delegate: ListItem {

                    height: modelLayout.height + (divider.visible ? divider.height : 0)
                          
                    ListItemLayout {
                        id: modelLayout
                        Row {
                            id: row
                            spacing: units.gu(1)
                                  
                            Icon {
                                width: units.gu(3)
                                height: units.gu(3)
                                color: Suru.foregroundColor
                                source: model.uid == 1 ? "file:///usr/share/icons/suru/actions/scalable/stock_music.svg" : "file:///usr/share/icons/suru/devices/scalable/media-optical-symbolic.svg"
                            }
                                  
                            Text {
                                anchors.verticalCenter: row.verticalCenter
                                color: Suru.foregroundColor
                                text: model.title
                            }
                        }
                    }
                    
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                          if(!contentView) mpd.search.getSongsFromAlbum(model.title)
                          contentView = true
                          searching = false
                        }
                    }
                    
                    trailingActions: ListItemActions {
                        actions: [
                            Action {
                                iconName: "media-playback-start"
                                text: i18n.tr("Play")
                                onTriggered: {
                                  mpd.queue.clear()
                                  if(mpd.search.add(model.title, model.uid == 1 ? 3 : 1)) {
                                    rectUserInfo.userInfoText = i18n.tr("Play album")
                                    mpd.control.play(1)
                                  } else {
                                    rectUserInfo.userInfoText = i18n.tr("Album could not be played")
                                  }
                                  infoTimer.start()
                                  rectUserInfo.showLabel = true                          
                                }
                            },
                            Action {
                                iconName: "media-playlist"
                                text: i18n.tr("Add")
                                onTriggered: {
                                  if(mpd.search.add(model.title, model.uid == 1 ? 3 : 1)) {
                                    rectUserInfo.userInfoText = i18n.tr("Album add to the queue")
                                  } else {
                                    rectUserInfo.userInfoText = i18n.tr("Album could not be added to playlist")
                                  }
                                  infoTimer.start()
                                  rectUserInfo.showLabel = true
                                }
                            }
                        ]
                    }
                }
                highlight: Rectangle { color: Suru.neutralColor; radius: 5 }
                highlightMoveDuration: 10
                highlightMoveVelocity: -1
                focus: true
            }
        }
        
//-------------------------------------------------------------------------------------------------    
/* Artists */
//-------------------------------------------------------------------------------------------------
        Item {
            width: tabView.width
            height: tabView.height
            opacity: tabView.currentIndex === 3 ? 3 : 0

            Behavior on opacity {
                NumberAnimation { duration: 200; easing.type: Easing.InOutCubic }
            }
            
            Item {
              id: itemArtistNavBar
              z: 1
              anchors.top: parent.top
              
              visible: contentView
              
              width: parent.width
              height: units.gu(6)
              
              property var widthLabelArtist: parent.width - units.gu(8)
              
              Rectangle {
                  width: parent.width
                  height: parent.height
                  color: UbuntuColors.orange
              }
              
              Row { //The "Row" type lays out its child items in a horizontal line
                spacing: units.gu(0.5)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                
                Icon {
                    width: units.gu(3)
                    height: units.gu(3)
                    color: Suru.foregroundColor
                    source: "file:///usr/share/icons/suru/actions/scalable/contact.svg"
                }

                Label {
                    id: labelActArtist
                      
                    width: itemArtistNavBar.widthLabelArtist
                    height: units.gu(3)
                    
                    color: Suru.foregroundColor
                    
                    verticalAlignment: Text.AlignVCenter

                    text: mpd.search.Name
                }
              }
            }
            
            ListView {
                id: artistView
                width: parent.width
                height: contentView ? parent.height - itemArtistNavBar.height : parent.height
                anchors.top: contentView ? itemArtistNavBar.bottom : parent.top                    
                model: mpd.search.albummodel
                      
                delegate: ListItem {

                    height: modelLayout.height + (divider.visible ? divider.height : 0)
                          
                    ListItemLayout {
                        id: modelLayout
                        Row {
                            id: row
                            spacing: units.gu(1)
                                  
                            Icon {
                                width: units.gu(3)
                                height: units.gu(3)
                                color: Suru.foregroundColor
                                source: model.uid == 1 ? "file:///usr/share/icons/suru/actions/scalable/stock_music.svg" : "file:///usr/share/icons/suru/actions/scalable/contact.svg"
                            }
                                  
                            Text {
                                anchors.verticalCenter: row.verticalCenter
                                color: Suru.foregroundColor
                                text: model.title
                            }
                        }
                    }
                    
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                          if(!contentView) mpd.search.getSongsFromArtist(model.title)
                          contentView = true
                          searching = false
                        }
                    }
                    
                    trailingActions: ListItemActions {
                        actions: [
                            Action {
                                iconName: "media-playback-start"
                                text: i18n.tr("Play")
                                onTriggered: {
                                  mpd.queue.clear()
                                  if(mpd.search.add(model.title, model.uid == 1 ? 4 : 2)) {
                                    rectUserInfo.userInfoText = i18n.tr("Play artist")
                                    mpd.control.play(1)
                                  } else {
                                    rectUserInfo.userInfoText = i18n.tr("Artist could not be played")
                                  }
                                  infoTimer.start()
                                  rectUserInfo.showLabel = true                          
                                }
                            },
                            Action {
                                iconName: "media-playlist"
                                text: i18n.tr("Add")
                                onTriggered: {
                                  if(mpd.search.add(model.title, model.uid == 1 ? 4 : 2)) {
                                    rectUserInfo.userInfoText = i18n.tr("Artist add to the queue")
                                  } else {
                                    rectUserInfo.userInfoText = i18n.tr("Artist could not be added to playlist")
                                  }
                                  infoTimer.start()
                                  rectUserInfo.showLabel = true
                                }
                            }
                        ]
                    }
                }
                highlight: Rectangle { color: Suru.neutralColor; radius: 5 }
                highlightMoveDuration: 10
                highlightMoveVelocity: -1
                focus: true
            }
        }
    }
    
    ListView {
        id: tabView
        anchors {
            top: parent.header.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        model: tabs
        orientation: Qt.Horizontal
        snapMode: ListView.SnapOneItem
        highlightRangeMode: ListView.StrictlyEnforceRange
        highlightMoveDuration: UbuntuAnimation.FastDuration

        onCurrentIndexChanged: {
            searching = false
            sections.selectedIndex = currentIndex
            if( currentIndex === 1 ) {
              contentView = false
              mpd.playlist.checkPlaylists()
            } else if( currentIndex === 2 ) {
              contentView = false
              mpd.search.getAlbumList()
            } else if( currentIndex === 3 ) {
              contentView = false
              mpd.search.getArtistList()
            }
        }
    }

    Component {
        id: removePlaylist
	
        Dialog {
            id: removePlaylistDialog
            title: i18n.tr("Remove playlist")
           
            Label { 
                text: contentView ? i18n.tr("Are you sure you want to remove the selected title from the playlist?") : i18n.tr("Are you sure you want to remove the selected playlist from the server?")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
            }
           
            Button {
                text: i18n.tr("delete")
                color: UbuntuColors.red
                onClicked: {
                    if(contentView) {
                        mpd.playlist.removeSong(listName, playlistPos)
                    } else {
                        mpd.playlist.remove(listName)
                    }
                  PopupUtils.close(removePlaylistDialog)
                }
            }
            
            Button {
                text: i18n.tr("cancel")
                color: UbuntuColors.ash
                onClicked: PopupUtils.close(removePlaylistDialog)
            }
        }
    }
  
    Timer {
        id: infoTimer 
        interval: 2000 //ms
        repeat: false
        onTriggered: rectUserInfo.showLabel = false
    }
    
    Rectangle {
        id: rectUserInfo
        
        color: UbuntuColors.warmGrey
        
        width: labelUserInfo.contentWidth + units.gu(3)
        height: labelUserInfo.contentHeight + units.gu(1)
        
        radius: units.gu(1)
        
        border.width: units.gu(0.1)
        border.color: UbuntuColors.graphite
        
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        
        property var showLabel: false
        property var userInfoText: ""

        visible: showLabel
        
        Label {
            id: labelUserInfo
            
            width: contentWidth
            
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter 
            
            visible: rectUserInfo.showLabel
            
            font.pointSize: units.gu(1.5)
            color: UbuntuColors.porcelain
            text: rectUserInfo.userInfoText
        }
    }
}
