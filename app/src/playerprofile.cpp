/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playerprofile.h"

PlayerProfile::PlayerProfile(QObject *parent) : QObject(parent)
{
  auto profilemodel = new ProfileModel(this);
  setProfilemodel(profilemodel);
  m_db = new QSqlDatabase();
  createDataDir();
  readAllEntrys();
}

PlayerProfile::~PlayerProfile()
{
    freeConnection();
    closeDB();
}

void PlayerProfile::setProfilemodel(ProfileModel* profilemodel)
{
  m_profilemodel = profilemodel;
}

ProfileModel * PlayerProfile::profilemodel()
{
  return m_profilemodel;
}

bool PlayerProfile::connectProfile(int id)
{
    QSqlQuery query(*m_db);
    query.prepare("SELECT * FROM profile WHERE id = (:id)");
    query.bindValue(":id", id);

    if (!query.exec()) {
        qDebug() << "exec :" << query.lastError();
        return false;
    } else {
        query.first();

        PlayerProfileSettings profile;
        profile.name = query.value(1).toString();
        profile.ip = query.value(2).toString();
        profile.port = query.value(3).toInt();
        profile.timeout_ms = query.value(4).toInt();
        profile.password = query.value(5).toString();

        return establishConnection(profile);
    }
}

bool PlayerProfile::establishConnection(PlayerProfileSettings profile)
{
    connection = mpd_connection_new(utils::QStringToBA(profile.ip).data(), profile.port, profile.timeout_ms);
    enum mpd_error error = mpd_connection_get_error(connection);

    if( error == MPD_ERROR_SYSTEM) {
        m_strError = _("Connection failed\nPlease check connection settings");
        emit strErrorChanged(m_strError);
        return false;
    }

    if( error == MPD_ERROR_TIMEOUT) {
        m_strError = _("Connection timeout\nPlease check network connection");
        emit strErrorChanged(m_strError);
        return false;
    }

    if( error == MPD_ERROR_SUCCESS) //Connection established
    {
        if( profile.password != "")
        {
            if( !mpd_run_password(connection, utils::QStringToBA(profile.password).data()))
            {
                m_strError = _("Connection failed\nPlease check the password");
                emit strErrorChanged(m_strError);
                freeConnection();
                return false;
            }
        }

        if(mpd_run_status(connection) == NULL) {
            m_strError = _("Successful connection\nNo read permission");
            emit strErrorChanged(m_strError);
            freeConnection();
            return false;
        }

        m_strError = "";
        emit strErrorChanged(m_strError);
        m_connected = true;
        emit statusConnectionChanged();
        m_strProfile = profile.name;
        emit strProfileChanged();
        emit connectionEstablished(connection);
        return error == MPD_ERROR_SUCCESS;
    } else {
        return false;
    }
}

void PlayerProfile::freeConnection(void)
{
    emit connectionClosed();
    mpd_connection_free(connection);
    m_connected = false;
    emit statusConnectionChanged();
}

void PlayerProfile::createDataDir()
{
  QString pathDatabase = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation) + QDir::separator() + "database";
  
  QDir dir;
  if( dir.mkpath(pathDatabase)) {
    createDB(pathDatabase);
  }
}

void PlayerProfile::createDB(QString path)
{
    QString url = path + "/mpd.sqlite";

    *m_db = QSqlDatabase::addDatabase("QSQLITE");
    m_db->setDatabaseName(url);
  
    if (!m_db->open())
      qDebug() << "Error opening DB: " << m_db->lastError().text();
    
    createTableProfile();
}

void PlayerProfile::createTableProfile()
{
    QSqlQuery query(*m_db);
    if( !query.exec("create table if not exists profile "
              "(id integer primary key autoincrement, "
              "name varchar(20), "
              "ip varchar(15), "
              "port integer, "
              "timeout integer, "
              "password varchar(20))") ) {
      qDebug() << "exec :" << query.lastError();
    }
}

void PlayerProfile::readAllEntrys()
{
  m_profilemodel->removeAllItems();
  m_noprofile = true;
  
  QSqlQuery query(*m_db);
  //Daten abfragen
  query.prepare("SELECT id, name, ip, port, timeout, password FROM profile ORDER BY id ASC");

  if (!query.exec()) {
    qDebug() << "exec :" << query.lastError();
  } else {
    while (query.next()) {
      m_noprofile = false;
      m_profilemodel->appendRow(query.value(0).toInt(),
                                query.value(1).toString(),
                                query.value(2).toString(),
                                query.value(3).toInt(),
                                query.value(4).toInt(),
                                query.value(5).toString());
    }
  }
  emit noprofileChanged(m_noprofile);
}

bool PlayerProfile::addProfile(QString name, QString ip, int port, int timeout, QString pw, bool connect)
{
    QSqlQuery query(*m_db);
    //Profil speichern
    query.prepare("INSERT INTO profile (name, ip, port, timeout, password) "
                    "VALUES (:name, :ip, :port, :timeout, :password)");
    query.bindValue(":name", name);
    query.bindValue(":ip", ip);
    query.bindValue(":port", port);
    query.bindValue(":timeout", timeout);
    query.bindValue(":password", pw);
    if( !query.exec())
        qDebug() << "exec :" << query.lastError();

    readAllEntrys();

    if( connect )
    {
        PlayerProfileSettings profile;
        profile.name = name;
        profile.ip = ip;
        profile.port = port;
        profile.timeout_ms = timeout;
        profile.password = pw;

        return establishConnection(profile);
    }

    return false;
}

bool PlayerProfile::editProfile(int id, QString name, QString ip, int port, int timeout, QString pw, bool connect)
{
  QSqlQuery query(*m_db);
  query.prepare("UPDATE profile SET name = :name, ip = :ip, port = :port, timeout = :timeout, password = :password WHERE "
                "id = :id");
  query.bindValue(":ip", ip);
  query.bindValue(":id", id);
  query.bindValue(":name", name);
  query.bindValue(":port", port);
  query.bindValue(":timeout", timeout);
  query.bindValue(":password", pw);
  if (!query.exec())
    qDebug() << "exec :" << query.lastError();
  
  readAllEntrys();

    if( connect )
    {
        PlayerProfileSettings profile;
        profile.name = name;
        profile.ip = ip;
        profile.port = port;
        profile.timeout_ms = timeout;
        profile.password = pw;

        return establishConnection(profile);
    }

    return false;
}

bool PlayerProfile::noprofile()
{
  return m_noprofile;
}

QString PlayerProfile::strProfile() const
{
  return m_strProfile;
}

void PlayerProfile::deleteProfile(int id)
{
  QSqlQuery query(*m_db);
  query.prepare("DELETE FROM profile WHERE id = :id");
  query.bindValue(":id", id);
  if (!query.exec())
    qDebug() << "exec :" << query.lastError();
  
  readAllEntrys();
}

void PlayerProfile::closeDB()
{
  m_db->close();
}

QString PlayerProfile::strError() const
{
 return m_strError;
}

bool PlayerProfile::isConnected() const
{
    return m_connected;
}
