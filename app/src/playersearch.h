/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYERSEARCH_H
#define PLAYERSEARCH_H

#include "playlistmodel.h"
#include "utils.h"
#include "connect.h"

class PlayerSearch : public Connect
{
  Q_OBJECT
  Q_PROPERTY(PlayListModel *albummodel READ albummodel WRITE setAlbummodel NOTIFY albummodelChanged);
  Q_PROPERTY(QString Name READ Name WRITE setName NOTIFY updateName);
  
public:
  PlayerSearch(QObject *parent = 0);

  PlayListModel *albummodel();
  void setAlbummodel(PlayListModel *albummodel);
  
  Q_INVOKABLE void getAlbumList(void);
  Q_INVOKABLE void getSongsFromAlbum(QString album);
  Q_INVOKABLE void forAlbum(QString album);
  Q_INVOKABLE bool add(QString album, unsigned tag);
  
  Q_INVOKABLE void getArtistList(void);
  Q_INVOKABLE void getSongsFromArtist(QString artist);
  Q_INVOKABLE void forArtist(QString artist);
  
  void setName(QString name);
  QString Name() const;
  
signals:
  void albummodelChanged(PlayListModel *albummodel);
  void updateName(QString name);
  
private:
  QStringList m_albumlist;
  QStringList m_artistList;
  QString m_name;
  
  PlayListModel *m_albummodel = nullptr;
};

#endif
