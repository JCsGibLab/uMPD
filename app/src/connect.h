/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONNECT_H
#define CONNECT_H

#include<QObject>
#include <QDebug>

#include "mpd/client.h"

class Connect: public QObject {
  Q_OBJECT

public:
  Connect(QObject *parent = 0);
  
  void getConnection(struct mpd_connection *connection);
  void closeConnection();
  bool isConnected();
  
  struct mpd_connection *m_connection;
  
signals:
    void connected();
    void disconnected();
    
private:
    bool m_isConnected = false;
};
#endif
