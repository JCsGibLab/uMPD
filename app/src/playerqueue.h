/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PLAYERQUEUE_H
#define PLAYERQUEUE_H

#include "utils.h"
#include "connect.h"

class PlayerQueue : public Connect
{
  Q_OBJECT
  
public:
  PlayerQueue(QObject *parent = 0);
  
  Q_INVOKABLE bool clear();
  Q_INVOKABLE bool deletePos(unsigned pos);
  Q_INVOKABLE bool add(QString uri);

signals:
  void queueChanged(void);
  
private slots:

private:

};

#endif
