/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "umpd.h"

const QString GETTEXT_DOMAIN = "umpd.stefanweng";

Umpd::Umpd()
{
  auto status = new PlayerState(this);
  setStatus(status);
  
  auto control = new PlayerControl(this);
  setControl(control);
  
  auto queue = new PlayerQueue(this);
  setQueue(queue);
  
  auto playlist = new PlayList(this);
  setPlaylist(playlist);
  
  auto database = new PlayerDatabase(this);
  setDatabase(database);
  
  auto stats = new PlayerStats(this);
  setStats(stats);
  
  auto search = new PlayerSearch(this);
  setSearch(search);
  
  auto profile = new PlayerProfile(this);
  setProfile(profile);
}

Umpd::~Umpd()
{
}

void Umpd::setStatus(PlayerState* status)
{
  m_status = status;
}

PlayerState * Umpd::status()
{
  return m_status;
}

void Umpd::setControl(PlayerControl* control)
{
  m_control = control;
  connect(m_control, &PlayerControl::volumeChanged,
          this, &Umpd::onVolumeChanged);
  connect(m_control, &PlayerControl::playingChanged,
          this, &Umpd::onPlayingChanged);
  connect(m_control, &PlayerControl::songChanged,
          this, &Umpd::onSongChanged);
  connect(m_control, &PlayerControl::repeatChanged,
          this, &Umpd::onRepeatChanged);
  connect(m_control, &PlayerControl::shuffleChanged,
        this, &Umpd::onShuffleChanged);
  connect(m_control, &PlayerControl::seekedPos,
        this, &Umpd::onSeekPosChanged);
}

PlayerControl * Umpd::control()
{
  return m_control;
}

void Umpd::setQueue(PlayerQueue* queue)
{
  m_queue = queue;
  connect(m_queue, &PlayerQueue::queueChanged,
          this, &Umpd::onQueueChanged);
}

PlayerQueue * Umpd::queue()
{
  return m_queue;
}

void Umpd::setPlaylist(PlayList* playlist)
{
  m_playlist = playlist;
}

PlayList * Umpd::playlist()
{
  return m_playlist;
}

void Umpd::setDatabase(PlayerDatabase* database)
{
  m_database = database;
}

PlayerDatabase * Umpd::database()
{
  return m_database;
}

void Umpd::setStats(PlayerStats* stats)
{
  m_stats = stats;
}

PlayerStats * Umpd::stats()
{
  return m_stats;
}

void Umpd::setSearch(PlayerSearch* search)
{
  m_search = search;
}

PlayerSearch * Umpd::search()
{
  return m_search;
}

void Umpd::setProfile(PlayerProfile* profile)
{
  m_profile = profile;
  connect(m_profile, SIGNAL(connectionEstablished(mpd_connection *)), this, SLOT(onConnectionEstablished(mpd_connection *)));
  connect(m_profile, SIGNAL(connectionClosed()), this, SLOT(onConnectionClosed()));
}

PlayerProfile * Umpd::profile()
{
  return m_profile;
}

void Umpd::onConnectionEstablished(struct mpd_connection *connection)
{
    m_status->getConnection(connection);
    m_control->getConnection(connection);
    m_queue->getConnection(connection);
    m_playlist->getConnection(connection);
    m_database->getConnection(connection);
    m_stats->getConnection(connection);
    m_search->getConnection(connection);
}

void Umpd::onConnectionClosed(void)
{
    m_status->closeConnection();
    m_control->closeConnection();
    m_queue->closeConnection();
    m_playlist->closeConnection();
    m_database->closeConnection();
    m_stats->closeConnection();
    m_search->closeConnection();
}

void Umpd::onPlayingChanged()
{
  m_status->checkPlaying();
}

void Umpd::onVolumeChanged()
{
  m_status->checkVolume();
}

void Umpd::onSongChanged()
{
  m_status->checkSong();
}

void Umpd::onRepeatChanged()
{
  m_status->checkRepeat();
  m_status->checkSingle();
}

void Umpd::onShuffleChanged()
{
  m_status->checkShuffle();
}

void Umpd::onSeekPosChanged()
{
  m_status->checkElapsedTime();
}

void Umpd::onQueueChanged()
{
  m_status->checkQueueVersion();
}
