/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playerstats.h"

PlayerStats::PlayerStats(QObject *parent) : Connect(parent)
{
}

void PlayerStats::update()
{
  struct mpd_stats *stats;

  stats = mpd_run_stats(m_connection);
  
  checkUpdateTime(stats);
  checkNumberOfArtists(stats);
  checkNumberOfAlbums(stats);
  checkNumberOfSongs(stats);
  checkPlayTime(stats);

  mpd_stats_free(stats);
}

void PlayerStats::checkUpdateTime(struct mpd_stats *stats)
{
  QDateTime timestamp;
  timestamp.setTime_t(mpd_stats_get_db_update_time(stats));
  setUpdateTime(timestamp.toString("dd.MM.yyyy hh.mm.ss"));
}

void PlayerStats::setUpdateTime(QString updateTime)
{
  if( updateTime != m_updateTime) {
    m_updateTime = updateTime;
    emit updateTimeChanged(m_updateTime);
  }
}

QString PlayerStats::updateTime() const
{
  return m_updateTime;
}

void PlayerStats::checkNumberOfArtists(struct mpd_stats* stats)
{
  setNumberOfArtists(mpd_stats_get_number_of_artists(stats));
}

void PlayerStats::setNumberOfArtists(unsigned int numArtists)
{
  if( numArtists != m_numArtists) {
    m_numArtists = numArtists;
    emit updateNumberOfArtists(m_numArtists);
  }
}

unsigned int PlayerStats::numberOfArtists() const
{
  return m_numArtists;
}

void PlayerStats::checkNumberOfAlbums(struct mpd_stats* stats)
{
  setNumberOfAlbums(mpd_stats_get_number_of_albums(stats));
}

void PlayerStats::setNumberOfAlbums(unsigned int numAlbums)
{
  if( numAlbums != m_numAlbums) {
    m_numAlbums = numAlbums;
    emit updateNumberOfAlbums(m_numAlbums);
  }
}

unsigned int PlayerStats::numberOfAlbums() const
{
  return m_numAlbums;
}

void PlayerStats::checkNumberOfSongs(struct mpd_stats* stats)
{
  setNumberOfSongs(mpd_stats_get_number_of_songs(stats));
}

void PlayerStats::setNumberOfSongs(unsigned int numSongs)
{
  if( numSongs != m_numSongs) {
    m_numSongs = numSongs;
    emit updateNumberOfSongs(m_numSongs);
  }
}

unsigned int PlayerStats::numberOfSongs() const
{
  return m_numSongs;
}

void PlayerStats::checkPlayTime(struct mpd_stats* stats)
{
  QDateTime timestamp;
  timestamp.setTime_t(mpd_stats_get_db_play_time(stats));
  setPlayTime(timestamp.toString("d hh.mm.ss"));
}

void PlayerStats::setPlayTime(QString playTime)
{
  if( playTime != m_playTime) {
    m_playTime = playTime;
    emit updatePlayTime(m_playTime);
  }
}

QString PlayerStats::playTime() const
{
  return m_playTime;
}
