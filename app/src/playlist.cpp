/*
 * Copyright (C) 2019 - Stefan Weng <stefwe@mailbox.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playlist.h"

PlayList::PlayList(QObject *parent) : Connect(parent)
{
  auto playmodel = new PlayListModel(this);
  setPlaymodel(playmodel);
  
  connect(this, SIGNAL(connected()), this, SLOT(onConnected()));
}

void PlayList::setPlaymodel(PlayListModel* playmodel)
{
  m_playmodel = playmodel;
}

PlayListModel * PlayList::playmodel()
{
  return m_playmodel;
}

void PlayList::onConnected()
{
    //Abfragen der gespeicherten Playlists
    checkPlaylists();
}

void PlayList::selectedEntity(QString playlistName, QString playlistNumber)
{
  qDebug() << "gewaehlte playlist:" << playlistName;
  if(playlistName != m_name) {
    m_playlistNum = playlistNumber.toInt();
    setPlaylist(m_playlistNum);
    setName(playlistName);
  }
  getContent(playlistName);
}
//TODO: Die Num könnte dem Model übergeben werden und beim Aufruf im qml mit übergeben werden
//Damit würde die Funktion entfallen
void PlayList::setPlaylist(int playlistNum)
{
  qDebug() << "aktuelle Nummer:" << playlistNum;
  emit selPlaylistChanged(playlistNum);
}

int PlayList::selPlaylist() const
{
  return m_playlistNum;
}

void PlayList::setName(QString name){
  if(name != m_name)
  {
    m_name = name;
    emit updateName(name);
  }
}

QString PlayList::name() const
{
  return m_name;
}

bool PlayList::saveAs(QString playlistName)
{
  if(playlistName != "") {
    bool stat = mpd_run_save(m_connection, utils::QStringToBA(playlistName).data());
    checkPlaylists();
    return stat;
  } else {
    return false;
  }
}

bool PlayList::addToQueue(QString name)
{
  return mpd_run_load(m_connection, utils::QStringToBA(name).data());
}

bool PlayList::remove(QString name)
{
  bool stat = mpd_run_rm(m_connection, utils::QStringToBA(name).data());
  checkPlaylists();
  return stat;
}

void PlayList::checkPlaylists()
{
  qDebug() << "Abfrage: " << mpd_send_list_playlists(m_connection);
  const struct mpd_playlist *playlist;

  QStringList entityType, savedPlaylists;
  
  struct mpd_entity *entity;

  while (entity = mpd_recv_entity(m_connection)) {
      
    if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_PLAYLIST)
    {
      playlist = mpd_entity_get_playlist(entity);
      savedPlaylists << mpd_playlist_get_path(playlist);
      entityType << "0";
    }
    mpd_entity_free(entity);
  }
  savedPlaylists.sort();
  m_playmodel->loadSearchList(entityType, savedPlaylists);
}

void PlayList::getContent(QString playlist) {
  
  mpd_send_list_playlist_meta(m_connection, utils::QStringToBA(playlist).data());
  
  QStringList entityType, playlistContent, title;
  
  struct mpd_entity *entity;
  const struct mpd_song *song;

  while (entity = mpd_recv_entity(m_connection)) {
      
    if (mpd_entity_get_type(entity) == MPD_ENTITY_TYPE_SONG)
    {
      song = mpd_entity_get_song(entity);
      playlistContent << mpd_song_get_uri(song);
      entityType << "1";
      title << mpd_song_get_tag(song, MPD_TAG_TITLE, 0);
    }
    mpd_entity_free(entity);
  }
  m_playmodel->loadDirlists(entityType, title, playlistContent);
}

void PlayList::removeSong(QString playlistName, unsigned pos)
{
  qDebug() << "Plalistname: " << playlistName;
  qDebug() << "Playlistposition: " << pos;
  mpd_run_playlist_delete(m_connection, utils::QStringToBA(playlistName).data(), pos);
  getContent(playlistName);
}

