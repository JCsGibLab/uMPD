# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the umpd.stefanweng package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: umpd.stefanweng\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-03-17 21:24+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../app/src/playerprofile.cpp:73
msgid ""
"Connection failed\n"
"Please check connection settings"
msgstr ""

#: ../app/src/playerprofile.cpp:79
msgid ""
"Connection timeout\n"
"Please check network connection"
msgstr ""

#: ../app/src/playerprofile.cpp:90
msgid ""
"Connection failed\n"
"Please check the password"
msgstr ""

#: ../app/src/playerprofile.cpp:98
msgid ""
"Successful connection\n"
"No read permission"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:39
msgid "connected"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:39 ../app/qml/pages/MainPage.qml:47
msgid "Connections"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:45 ../app/qml/pages/DatabasePage.qml:37
#: ../app/qml/pages/DatabasePage.qml:43 ../app/qml/pages/AboutPage.qml:33
msgid "Back"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:56 ../app/qml/pages/DatabasePage.qml:63
#: ../app/qml/pages/MainPage.qml:59 ../app/qml/pages/AboutPage.qml:27
#: ../app/qml/pages/AboutPage.qml:47
msgid "About"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:65
#: ../app/qml/pages/ConnectionPage.qml:91
#: ../app/qml/components/NoProfile.qml:55
msgid "Add new profile"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:91
msgid "Edit profile"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:99
msgid "Profile name:"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:109
msgid "e.g. living room"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:121
msgid "Ip Address:"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:141
msgid "Port number:"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:161
msgid "Timeout (ms):"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:180
msgid "Password:"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:194 ../app/qml/pages/MainPage.qml:138
msgid "save"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:209
msgid "save and connect"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:229
#: ../app/qml/pages/ConnectionPage.qml:370
#: ../app/qml/pages/DatabasePage.qml:899 ../app/qml/pages/MainPage.qml:111
#: ../app/qml/pages/MainPage.qml:148
msgid "cancel"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:292
#: ../app/qml/pages/ConnectionPage.qml:358
#: ../app/qml/pages/DatabasePage.qml:493 ../app/qml/pages/DatabasePage.qml:886
#: ../app/qml/pages/MainPage.qml:194
msgid "delete"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:306
msgid "Edit"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:349
msgid "Remove profile"
msgstr ""

#: ../app/qml/pages/ConnectionPage.qml:352
msgid "Are you sure you want to delete the selected profile?"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:37 ../app/qml/pages/MainPage.qml:40
msgid "Database"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:73
msgid "Update"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:84
msgid "Search"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:111
msgid "Folder"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:112
msgid "Playlists"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:113
msgid "Albums"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:114
msgid "Artists"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:151
msgid "Search by album..."
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:151
msgid "Search by artist..."
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:168
msgid "Database statistic"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:171
msgid "Last Database update:"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:177
msgid "Number of Artists:"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:183
msgid "Number of Albums:"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:189
msgid "Number of Songs:"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:195
msgid "Total playtime:"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:202
msgid "Update database"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:207
msgid "Update done"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:212
msgid "Update failed"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:218
msgid "back"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:350 ../app/qml/pages/DatabasePage.qml:511
#: ../app/qml/pages/DatabasePage.qml:667 ../app/qml/pages/DatabasePage.qml:805
msgid "Play"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:354 ../app/qml/pages/DatabasePage.qml:516
msgid "Add to the queue and start playing"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:357 ../app/qml/pages/DatabasePage.qml:519
msgid "Load and playback not possible"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:365 ../app/qml/pages/DatabasePage.qml:535
#: ../app/qml/pages/DatabasePage.qml:682 ../app/qml/pages/DatabasePage.qml:820
msgid "Add"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:368 ../app/qml/pages/DatabasePage.qml:539
msgid "Add to the queue"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:370 ../app/qml/pages/DatabasePage.qml:541
msgid "Not able to add to queue"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:523
msgid "Play playlist"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:526
msgid "Playlist could not be played back"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:545
msgid "Playlist add to the queue"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:547
msgid "Playlist could not be added to playlist"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:671
msgid "Play album"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:674
msgid "Album could not be played"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:685
msgid "Album add to the queue"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:687
msgid "Album could not be added to playlist"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:809
msgid "Play artist"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:812
msgid "Artist could not be played"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:823
msgid "Artist add to the queue"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:825
msgid "Artist could not be added to playlist"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:877
msgid "Remove playlist"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:880
msgid "Are you sure you want to remove the selected title from the playlist?"
msgstr ""

#: ../app/qml/pages/DatabasePage.qml:880
msgid "Are you sure you want to remove the selected playlist from the server?"
msgstr ""

#: ../app/qml/pages/MainPage.qml:66
msgid "Clear Queue"
msgstr ""

#: ../app/qml/pages/MainPage.qml:73
msgid "Save Queue"
msgstr ""

#: ../app/qml/pages/MainPage.qml:92
msgid "Queue"
msgstr ""

#: ../app/qml/pages/MainPage.qml:95
msgid "Are you sure you want to clear the queue?"
msgstr ""

#: ../app/qml/pages/MainPage.qml:101
msgid "clear"
msgstr ""

#: ../app/qml/pages/MainPage.qml:123
msgid "Save queue as playlist"
msgstr ""

#: ../app/qml/pages/MainPage.qml:126
msgid "Please enter the name under you want to save the queue"
msgstr ""

#: ../app/qml/pages/MainPage.qml:134
msgid "e.g. favourite songs"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:48
msgid "Support"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:106
msgid "uMPD"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:113
msgid "Version %1"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:120
msgid "This application lets you control a music player daemon"
msgstr ""

#. TRANSLATORS: Please make sure the URLs are correct
#: ../app/qml/pages/AboutPage.qml:129
msgid ""
"Released under the terms of the <a href=\"https://gitlab.com/StefWe/uMPD/"
"blob/master/LICENSE\">GNU GPL v3</a>"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:139
msgid "Copyright"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:161
msgid "Get the source"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:162
msgid "Report issues"
msgstr ""

#: ../app/qml/pages/AboutPage.qml:163
msgid "Help translate"
msgstr ""

#: ../app/qml/components/NoProfile.qml:65
msgid "Click the 'plus' at the top to add a mpd server profile"
msgstr ""
